from sys import argv
import json
import requests

from db.helper import get_unsync_log, set_sync_log

# unpack cli argument
filename, url = argv

# get unsync logs
logs = list(map(dict, get_unsync_log()))

# send request to server to save logs
body = json.dumps(logs)
print('send request to {} with data {}'.format(url, body))
res = requests.post(url, data={'data': body})

if res.status_code == 200:

    # set flag sync in local log if success
    ids = set([log['id'] for log in logs])
    set_sync_log(ids)

    print('sync with server success')

else:
    print('sync failed')
    print(res.status_code)

