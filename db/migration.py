import sqlite3


def run_migration(db_name):

    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    print('starting migrations in {} ...'.format(db_name))

    # create log table
    c.execute('''CREATE TABLE miflora_log ( id integer primary key,
            source text, firmware text, battery int, 
            moisture int, conductivity int, light int, temperature real,
            timestamp text, sync int
            )'''
    )

    conn.commit()
    conn.close()

    print('migrations completed')