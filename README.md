## Miflora Log Reader

### Requirements
* Python3
* SQLite

### Installation
* $ pip3 install -r requirements.txt
* edit default configuration on setup.py
```
DB_NAME = 'local.db'
LOG_FREQUENCY = 1   # in minutes
SYNC_FREQUENCY = 3  # in hours
SYNC_SERVER_URL = 'http://localhost:80/logs'
```
* $ python3 setup.py
    * setup db migration (sqlite)
    * set job scheduling with cronjob