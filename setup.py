import os

from db.migration import run_migration
from crontab import CronTab


DB_NAME = 'local.db'
LOG_FREQUENCY = 1   # in minutes
SYNC_FREQUENCY = 3  # in hours
SYNC_SERVER_URL = 'http://localhost:80/logs?secret_flag=flag' # POST default


def setup():

    print('Init db & do migrations...')
    try:
        run_migration(DB_NAME)
    except Exception as e:
        print(e)

    print('\nSetup job scheduling in Cronjob...')
    cron = CronTab(user=True)
    # clear previous job to prevent duplicate
    cron.remove_all()
    log_job = cron.new(command='cd %s && /usr/bin/python3 logger.py' % os.getcwd())
    sync_job = cron.new(command='cd %s && /usr/bin/python3 sync.py %s' % (os.getcwd(), SYNC_SERVER_URL))
    log_job.minute.every(LOG_FREQUENCY)
    sync_job.hour.every(SYNC_FREQUENCY)
    cron.write()

    print('\nSetup Finished!')

if __name__ == '__main__':
    setup()